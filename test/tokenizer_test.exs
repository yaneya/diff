defmodule TokenizerTest do
  use ExUnit.Case
  doctest Tokenizer
  
  test "basic split" do
    r = {:ok, ["1", "2", "3"]}
    assert r === Tokenizer.tokenize("1 2 3")
  end
  
  test "empty split" do
    assert {:ok,  []} === Tokenizer.tokenize("")
  end

  test "new line separator split" do
    r = {:ok, ["1", "2", "\r\n", "3"]}
    assert r === Tokenizer.tokenize("1 2\r\n3")
  end

  test "new line separator surrounded spaces" do
    r = {:ok, ["1", "2", "\r\n", "", "3"]}
    assert r === Tokenizer.tokenize("1 2 \r\n 3")
  end

  test "new line separator before space" do
    r = {:ok, ["1", "2", "\r\n", "", "3"]}
    assert r === Tokenizer.tokenize("1 2\r\n 3")
  end

  test "without split" do
    assert {:ok, ["1"]} === Tokenizer.tokenize("1")
  end

  test "the truth" do
    assert 1 + 1 == 2
  end
end
